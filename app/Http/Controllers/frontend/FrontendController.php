<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home.index');
    }

    public function acerca_de()
    {
        return view('frontend.acerca-de');
    }

    public function contacto()
    {
        return view('frontend.contacto');
    }

    public function libreria()
    {
        return view('frontend.libreria');
    }
    
    public function dashboard()
    {
        return view('escritorio.index');
    }

    public function mis_suscripciones()
    {
        return view('escritorio.representante.suscripciones');
    }
    public function listado_tareas()
    {
        return view('escritorio.representante.tareas');
    }
    public function listado_tests()
    {
        return view('escritorio.representante.tests');
    }
}