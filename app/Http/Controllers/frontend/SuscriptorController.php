<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/*
use App\Models\User;
use App\Models\Productor_producto;
use App\Models\Necesidad;
use App\Models\Colaboracion;
use App\Models\Productor;
use App\Models\Comentario;
*/

use App\Http\Requests\StoreUserPost;
use App\Http\Requests\UpdateUserPut;
use Socialite;

use App\Mail\VerifyMail;
use App\Mail\ResetPasswordMail;
use App\Jobs\SendEmail;
use Mail;

class SuscriptorController extends Controller
{
    /**
     * Pantalla de login
     *
     * @return \Illuminate\Http\Response
     */
    public function entrar()
    {
        return view('auth.login');
    }
    
    /**
     * Iniciar session.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        /*

        $credentials = $this->validate(request(), [
            'email' => 'email|required|string',
            'password' => 'required|string'
        ]);

        if(Auth::attempt($credentials)){
            // if(!Auth::user()->hasVerifiedEmail()){
            //     return redirect()->route('verificar-cuenta');
            // }
            if(Auth::user()->isAdmin == 1){
                return redirect()->route('admin')->with('status', 'Usuario administrador logeado exitosamente.');
            } else {
                return redirect()->route('home')->with('status', 'Usuario logeado exitosamente.');
            }
        }

        return back()
            ->withErrors(['email' => trans('auth.failed')])
            ->withInput(request(['email']));
        
        */

        return redirect()->route('dashboard')->with(
            'status', 'Bienvenido'
        );
        
        
    }
    
    // /**
    //  * Iniciar session desde pop up necesidad.
    //  *
    //  * @param Request $request
    //  * @return \Illuminate\Http\Response
    //  */

    // public function loginNecesidad(Request $request)
    // {
    //     // $credentials = $this->validate(request(), [
    //     //     'email' => 'email|required|string',
    //     //     'password' => 'required|string'
    //     // ]);

    //     // if(Auth::attempt($credentials)){
    //     //     return redirect()->back()->with('status', 'Usuario logeado exitosamente.');
    //     // }

    //     // return back()
    //     //     ->withErrors(['email' => trans('auth.failed')])
    //     //     ->withInput(request(['email']));
        
    //     $email = $_POST['email'];
    //     $passw = Hash::make($_POST['password']);

    //     if(Auth::attempt(['email' => $email, 'password' => $passw])){
    //         return response()->json('Login exitoso', 200);
    //     }

    //     return response()->json('No hemos podido encontrar estas credenciales', 400);
        
    // }

    /**
     * Cerrar session.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();
        session()->flush();
        return redirect()->route('home');
    }




    /**
     * Pantalla de crear usuario.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.suscriptors.crear-cuenta');
    }

    /**
     * Crear un nuevo usuario del sistema.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'El campo :attribute es requerido.',
            'string' => 'El campo :attribute debe ser textual.',
            'confirmed' => 'Los campos :attribute deben coincidir.',
            'min' => 'El campo :attribute debe contener al menos :min caracteres.',
            'unique' => 'Ya tenemos registrado ese nombre de usuario',
          ];
          $validator = Validator::make($request->all(), StoreUserPost::rules(), $messages);
          if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
          }
    
          $user = new User;
          $user->nombre = $request->nombre;
          $user->telefono = $request->telefono;
          $user->email = $request->email;
        //   $user->username = $request->username;
          $user->password = Hash::make($request->password);
          $user->canton_id = $request->ciudad;
          $user->parroquia_id = $request->barrio ? $request->barrio : null;
          $user->verification_token = sha1($user);
          $user->save();

          try {
            Mail::to($user->email)->send(new VerifyMail($user));
            // SendEmail::dispatch($user);

            return redirect()->route('login-suscriptor')->with('status', 'Usuario creado con exito.');
          } catch (exception $e) {
            return redirect()->route('login-suscriptor')->with('status', 'Usuario creado con exito.')->withErrors('No hemos podido enviar el correo de verificacion, porfavor reporta este inconveniente.');
          }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function thankYou()
    {
        return view('frontend.suscriptors.thankyou-page');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function verify($id, $token)
    {
        $user = User::find($id);

        if($user == null){
            return back()->withErrors('No encontramos este articulo.');
        }

        if($user->verification_token != null && $user->verification_token == $token){
            if($user->hasVerifiedEmail()){
                return redirect()->route('login-suscriptor')->with('status', 'Este email ya esta verificado.');
            }

            if($user->markEmailAsVerified()){

                $user->verification_token = null;
                $user->save();

                return redirect()->route('login-suscriptor')->with('status', 'Email verificado correctamente.');
            }
        } else {
            return redirect()->route('home')->withErrors('Este email no coincide con nuestros datos.');
        }
    }

    /**
     * Pantalla de reenviar mensaje de validacion.
     *
     * @return \Illuminate\Http\Response
     */

    public function showVerificar(Request $request)
    {
        return view('auth.verify');
    }

    /**
     * Reenvia el mensaje de validacion.
     *
     * @return \Illuminate\Http\Response
     */

    public function resend()
    {
        if(!Auth::check()){
            return redirect()->route('login-suscriptor')->with('status', 'No hemos encontrado un usuario logeado.');
        }

        $user = User::find(Auth::id());

        if($user->hasVerifiedEmail()){
            return redirect()->route('login-suscriptor')->with('status', 'Este email ya esta verificado.');
        }

        $user->verification_token = sha1($user);
        $user->save();

        Mail::to($user->email)->send(new VerifyMail($user));

        Auth::logout();

        return redirect()->route('login-suscriptor')->with('status', 'Hemos reenviado el correo de verificacion.');
    }




    /**
     * pantalla para pedir cambio de clave, correo.
     *
     * @return \Illuminate\Http\Response
     */
    public function requestPassword()
    {
        return view('frontend.suscriptors.password-request')->with(
            [
                'logstatus'     => 'true',
                'valores' => $_POST,
                //'referer'       => $pageUrl
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function requestPasswordPost(Request $request)
    {
        $messages = [
            'required' => 'El campo :attribute es requerido.',
            'string' => 'El campo :attribute debe ser textual.',
            'exists' => 'No tenemos registrado ese email.',
        ];
        $validator = Validator::make($request->all(), [
            'email' => 'email|required|string|exists:users,email',
        ], $messages);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = User::where('email', $request->email)->first();

        $user->verification_token = sha1($user);
        $user->save();

        Mail::to($user->email)->send(new ResetPasswordMail($user));
        return redirect()->route('login-suscriptor')->with('status', 'Hemos enviado un correo de cambio de contraseña.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword($id, $token)
    {
        $user = User::find($id);
        
        if($user == null){
            return back()->withErrors('No encontramos este articulo.');
        }
        Auth::login($user);
        
        if($user->verification_token == $token){
            return view('frontend.suscriptors.password-recovery')->with(
                [
                    'logstatus'     => 'true',
                    'valores' => $_POST,
                    //'referer'       => $pageUrl
                ]
            );
        } else {
            return redirect()->route('login-suscriptor')->withErrors('No encontramos este email en nuestros registros.');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePasswordPost(Request $request)
    {
        $credentials = $this->validate(request(), [
            'password' => 'required|string|confirmed|min:7'
        ]);

        if(!Auth::check()){
            return redirect()->route('login-suscriptor')->withErrors('No hrmos podido encontrar un usuario logeado.');
        }

        $user = User::find(Auth::id());
        $user->password = Hash::make($request->password);
        $user->save();

        Auth::logout();
        
        return redirect()->route('login-suscriptor')->with('status', 'Cambio de contraseña exitoso.');
    }

    


    /**
     * Pantalla para crear cuenta de productor.
     *
     * @return \Illuminate\Http\Response
     */
    // public function createProveedor()
    // {
    //     return view('frontend.suscriptors.crear-proveedor');
    // }

    /**
     * Pantalla para crear productos del productor.
     *
     * @return \Illuminate\Http\Response
     */
    // public function addProducts(Request $request)
    // {
    //     if( !$request->session()->has('productor_id') ){
    //         return redirect()->route('home')->with('status', "No hallamos un productor");
    //     }

    //     $productor_id = session('productor_id');
    //     $productos = Productor_producto::where('productor_id', $productor_id)->get();
    //     return view('frontend.suscriptors.agregar-productos')
    //         ->with(['productos'  => $productos]);
    // }




    /**
     * Login usando facebook y google
     */
    public function redirectToProvider($provider=null)
    {
        //if(!config("services.$provider")) abort('404');
        
        return Socialite::driver($provider)->redirect();
    }


    public function handleProviderCallback($provider=null)
    {
        if($socialUser = Socialite::driver($provider)->stateless()->user()){
            // dd($socialUser);

            $user = User::where('social_id', $socialUser->getId())->first();

            if(!$user){
                $user = new User;
                $user->nombre = $socialUser->getName();
                $user->email = $socialUser->getEmail();
                $user->email_verified_at = date('Y-m-d H:i:s');
                $user->username = $socialUser->getName();
                $user->social_id = $socialUser->getId();
                $user->social_provider = $provider;
                $user->save();
            }

            if(!Auth::check()){
                // Auth::attempt(['email' => $email, 'password' => $password]);
                Auth::login($user, true);
            }

            return redirect()->route('home');
        }else{
            return redirect()->route('login-suscriptor')->with('status', 'Algo salio mal.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit()
    // {
    //     $user = User::find(Auth::id());
    //     return view('frontend.suscriptors.modificar-cuenta')
    //         ->with(['user' => $user]);
    // }

    /**
     * Modificar el perfil del usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request)
    // {
    //     //
    //     $messages = [
    //         'required' => 'El campo :attribute es requerido.',
    //         'string' => 'El campo :attribute debe ser textual.',
    //         'max' => 'El campo :attribute debe contener maximo :max caracteres.',
    //     ];
        
    //     $validator = Validator::make($request->all(), UpdateUserPut::rules(), $messages);
    //     if ($validator->fails()) {
    //         return back()->withErrors($validator)->withInput();
    //     }


    //     $user = User::find(Auth::id());
    //     $user->nombre = $request->nombre;
    //     $user->telefono = $request->telefono;
    //     $user->canton_id = $request->ciudad;
    //     $user->parroquia_id = $request->barrio ? $request->barrio : null;
    //     $user->save();

    //     return redirect()->route('edit.mi-perfil')->with('status', 'Usuario modificado con exito.');

    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $user = User::find($id);

    //     if(Auth::id()){
    //         if(Auth::id() != $user->id && Auth::user()->isAdmin == 0){
    //             return back()->with('status', 'Acceso restringido.');
    //         }
    //     } else {
    //         return back()->with('status', 'No encontramos un usuario logeado.');
    //     }

        
    //     Necesidad::where('user_id', $id)->delete();
    //     Colaboracion::where('user_id', $id)->delete();
    //     Productor::where('user_id', $id)->delete();

    //     $user->delete();
    //     return redirect()->route('crear-cuenta')->with('status', 'Usuario eliminado con exito');
    // }

    public function adminUsuarios()
    {
        return view('admin.administrar-usuarios')->with($user);
    }
}
