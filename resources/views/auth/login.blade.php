
@extends('layouts.frontend.app')
@section('content')
<main id="main">
	<div class="breadcrumbs" data-aos="fade-in">
		<div class="container">
		  <h2>Entrar</h2>
		  <p>Est dolorum ut non facere possimus quibusdam eligendi voluptatem. Quia id aut similique quia voluptas sit quaerat debitis. Rerum omnis ipsam aperiam consequatur laboriosam nemo harum praesentium. </p>
		</div>
	</div><!-- End Breadcrumbs -->
	<div class="container mb-5">
		<div class="row">
			<div class="col-md-4 offset-md-4">
				<form class="login-form" action="{{ route('post.login-suscriptor') }}" method="post" role="form">
					@csrf
					<div class="login-wrap">
						<p class="login-img text-center mt-5 fs-48"><i class="ri-lock-line"></i></p>
						<div class="input-group">
							<span class="input-group-addon fs-28 mr-3 d-inline"><i class="ri-shield-user-line"></i></span>
							<input name="email" type="text" class="form-control" placeholder="Usuario" autofocus required>
						</div>
						<div class="input-group">
							<span class="input-group-addon fs-28 mr-3 d-inline"><i class="ri-key-fill"></i></span>
							<input name="password" type="password" class="form-control" placeholder="Contraseña" required>
						</div>
						<label class="checkbox mt-4 mb-3">
							<input type="checkbox" value="remember-me"> Recordarme
							<span class="pull-right"> <a href="#">¿Olvidó su contraseña?</a></span>
						</label>
						<button class="btn btn-lg btn-success btn-block" type="submit">Entrar</button>
						<button class="btn btn-lg btn-info btn-block" type="submit">Regisrtrarse</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</main>
@endsection
    