@extends('escritorio.layout.app')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<h1 class="bolder uppercase">Escritorio</h1>
		<hr />
	</div>
</div>
<div class="row mb-5">
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-12 mb-3">
				<h3 class="bolder uppercase">Libros Activos</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 mb-4">
				<div class="course-item">
					<a href="#"><img src="assets/img/course-1.jpg" class="img-fluid" alt="..."></a>
					<div class="course-content">
						<a href="#"><h3>Musicalia</h3></a>
						<p>Et architecto provident deleniti facere repellat nobis iste. </p>
						<a class="btn bkg-yellow uppercase bolder text-white" href="#">Ver detalle</a>
					</div>
				</div>
			</div> <!-- End Course Item-->
			<div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 mb-4">
				<div class="course-item">
					<a href="#"><img src="assets/img/course-3.jpg" class="img-fluid" alt="..."></a>
					<div class="course-content">
					  <a href="#"><h3>Musicalia</h3></a>
					  <p>Et architecto provident deleniti facere.</p>
					  <a class="btn bkg-yellow uppercase bolder text-white" href="#">Ver detalle</a>
					</div>
				  </div>
			</div>
			<div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 mb-4">
				<div class="course-item">
					<a href="#"><img src="assets/img/course-2.jpg" class="img-fluid" alt="..."></a>
					<div class="course-content">
					  <a href="#"><h3>Musicalia</h3></a>
					  <p>Et architecto provident deleniti facere repellat nobis iste. Id facere quia quae dolores dolorem tempore.</p>
					  <a class="btn bkg-yellow uppercase bolder text-white" href="#">Ver detalle</a>
					</div>
				  </div>
			</div>
			<div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 mb-4">
				<div class="course-item">
					<a href="#"><img src="assets/img/course-3.jpg" class="img-fluid" alt="..."></a>
					<div class="course-content">
					  <a href="#"><h3>Musicalia</h3></a>
					  <p>Id facere quia quae dolores dolorem tempore.</p>
					  <a class="btn bkg-yellow uppercase bolder text-white" href="#">Ver detalle</a>
					</div>
				  </div>
			</div>
		</div>
	</div>
</div>
<div class="row mb-5">
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-12 mb-3">
				<h3 class="bolder uppercase">Librería</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 mb-4">
				<div class="course-item">
				  <a href="#"><img src="assets/img/course-3.jpg" class="img-fluid" alt="..."></a>
				  <div class="course-content">
					<a href="#"><h3 class="fs-18 mt-3 mb-3">Musicalia</h3></a>
					<a class="btn bkg-green uppercase bolder text-white" href="#">Activar</a>
				  </div>
				</div>
			</div> <!-- End Course Item-->
			<div class="col-lg-2 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 mb-4">
				<div class="course-item">
					<a href="#"><img src="assets/img/course-2.jpg" class="img-fluid" alt="..."></a>
					<div class="course-content">
					  <a href="#"><h3 class="fs-18 mt-3 mb-3">Musicalia</h3></a>
					  <a class="btn bkg-green uppercase bolder text-white" href="#">Activar</a>
					</div>
				  </div>
			</div>
			<div class="col-lg-2 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 mb-4">
				<div class="course-item">
					<a href="#"><img src="assets/img/course-1.jpg" class="img-fluid" alt="..."></a>
					<div class="course-content">
					  <a href="#"><h3 class="fs-18 mt-3 mb-3">Musicalia</h3></a>
					  <a class="btn bkg-green uppercase bolder text-white" href="#">Activar</a>
					</div>
				  </div>
			</div>
			<div class="col-lg-2 col-md-6 d-flex align-items-stretch mt-4 mt-md-0 mb-4">
				<div class="course-item">
					<a href="#"><img src="assets/img/course-3.jpg" class="img-fluid" alt="..."></a>
					<div class="course-content">
					  <a href="#"><h3 class="fs-18 mt-3 mb-3">Musicalia</h3></a>
					  <a class="btn bkg-green uppercase bolder text-white" href="#">Activar</a>
					</div>
				  </div>
			</div>
		</div>
	</div>
</div>
@endsection