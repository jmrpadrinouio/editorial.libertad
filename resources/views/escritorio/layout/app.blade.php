<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="descriptison">
    <meta content="" name="keywords">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
    <!-- Stylesheets -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/main-styles.css')}}">
    <link rel="stylesheet" href="{{asset('css/plataforma-styles.css')}}">
    <!-- END Stylesheets -->
    <script src="{{asset('js/app.js')}}"></script>
    <!-- Scripts -->
    <title>Editorial Libertad @yield('page_title')</title>
    @yield('head')
</head>
<body class="plataforma">

<!-- ERRORS ALERT -->
<div class="error-container" id="errorDiv">
{{-- @include('includes.validation-error') --}}
{{-- @include('includes.session-flash-status') --}}
{{-- {{ json_encode(session()->all()) }} --}}
{{-- {{ json_encode(Auth::user()) }} --}}
</div>

@yield('body_afteropeningtag')
@include('escritorio.layout.header')
@yield('content_before')
<div class="page-container">
    <main id="main" data-aos="fade-in">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 bg-light text-dark sidebar pt-5 pl-5">
                    <ul class="list-no-style no-padding">
                        <li class="fs-18 bolder text-blue"><i class="ri-timer-2-line"></i> <span id="clock">15:09</span></li>
                        <li class="fs-18 bolder text-blue"><i class="ri-calendar-line"></i> <?php echo date('d/m/Y')?></li>
                        <li class="bolder text-aqua mt-4"><i class="ri-user-fill"></i> José León</li>
                        <li><i class="ri-community-fill"></i> Colegio Benalcazar</li>
                        <li><i class="ri-map-pin-fill"></i> <span>Quito</span></li>
                    </ul>
                    <h2 class="fs-22 mb-3 bolder uppercase mt-4">Menu</h2>
                    <ul class="list-no-style no-padding">
                        <li><a href="/escritorio/mis-suscripciones">Mis suscripciones</a></li>					
                        <li><a href="/mi-perfil">Mi perfil</a></li>
                    </ul>
                    <hr />
                    <h2 class="fs-22 mb-3 bolder uppercase">Cartelera</h2>
                    <ul class="list-no-style no-padding">
                        <li class="mb-4"><a href="#">Disposición del Ministerio de Educación 2020</a></li>					
                        <li class="mb-4"><a href="#">Medidas preventicas contra el COVID-19</a></li>
                        <li class="mb-4"><a href="#">Nuevo regimen educativo Sierra</a></li>
                        <li class="mb-4"><a href="#">Nuevo regimen educativo Costa</a></li>
                    </ul>
                </div>
                <div class="col-md-10 content pt-5">
                    @yield('content')
                </div>
            </div>
        </div>
    </main>
</div>
</div><!-- End page Container -->
@yield('content_after')
@include('escritorio.layout.footer')
@yield('body_beforeclosingtag')
</body>
</html>
