
<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>Editorial Libertad</span></strong>. Todos los derechos reservados
        </div>
        
      </div>
      <div class="text-center text-md-right pt-3 pt-md-0">
        <div class="credits">
          Desarrollado por <a href="https://bootstrapmade.com/">CodeMotion</a> y Diseñado por <a href="https://bootstrapmade.com/">Choclomedia</a>
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  
@yield('footer_script_before')
{{-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script> --}}
<script src="{{ asset('assets/js/main.js') }}"></script>
@yield('footer_scripts')
<script>
    var baseUrl = '{{ URL::to('/') }}';

	function currentTime() {
		var date = new Date(); /* creating object of Date class */
		var hour = date.getHours();
		var min = date.getMinutes();
		var sec = date.getSeconds();
		var midday = "AM";
		midday = (hour >= 12) ? "pm" : "am"; /* assigning AM/PM */
		hour = (hour == 0) ? 12 : ((hour > 12) ? (hour - 12): hour); /* assigning hour in 12-hour format */
		hour = updateTime(hour);
		min = updateTime(min);
		sec = updateTime(sec);
		document.getElementById("clock").innerText = hour + " : " + min + " : " + sec + " " + midday; /* adding time to the div */
			var t = setTimeout(currentTime, 1000); /* setting timer */
		}

		function updateTime(k) { /* appending 0 before time elements if less than 10 */
		if (k < 10) {
			return "0" + k;
		}
		else {
			return k;
		}
	}

	currentTime();	
</script>
@yield('footer_script_after')
@yield('footer_after')