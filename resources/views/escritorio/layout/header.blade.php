@php
    //$menu_items = config('sidebarPublico.menu');
    //$menu_perfil = config('sidebarPublico.menu_perfil');
    $path = (Request::path() != '/') ? '/'. Request::path() : '/';
    $user = auth()->user();
@endphp

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
	<!--
    <div id="secondary-nav" class="secondary-nav">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <ul class="d-flex w-100 justify-content-between no-margin no-padding padding-tb-18 list-no-style">
                        <li><a href="/acerca-de">Acerca de</a></li>
                        <li><a href="/trabaja-con-nosotros">Trabaja con Nosotros</a></li>
                        <li><a href="/contacto">Contacto</a></li>
                    </ul>
                </div>
                <div class="col-lg-8 padding-tb-18 text-right">
                    <a href="/salir">Salir</a>
                </div>
            </div>
        </div>
	</div>
	-->	
    <div class="container-fluid d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="/">Nombre del sistema</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="/escritorio">Escritorio</a></li>
          <li><a href="/escritorio/listado-de-tareas">Tareas</a></li>
          <li><a href="/escritorio/listado-de-tests">Tests</a></li>
          <li class="drop-down open-left"><a href="">Jose</a>
            <ul>
              <li><a href="/mi-perfil">Mi perfil</a></li>
              <li><a href="/cambiar-clave">Cambiar contraseña</a></li>              
              <li><a href="/salir">Salir</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- .nav-menu -->
	  <!--
	  <a href="/registrarse" class="get-started-btn">Registrarse</a>
	  -->

    </div>
  </header><!-- End Header -->