@extends('escritorio.layout.app')
@section('content')
<div class="row mb-4">
	<div class="col-sm-12">
		<h1 class="bolder uppercase">Evolución de los Tests</h1>
	</div>
</div>
<div class="row mb-5">
	<div class="col-sm-12">
        <!-- Empieza el loop -->
		<div class="row mb-5 mt-5 mt-md-0 mb-md-0 pt-2 pb-2 profile-item-list">
			<div class="col-sm-12 col-md-1">
				<img class="img-fluid rounded-circle profile-circle-female" src="https://www.amcharts.com/wp-content/uploads/2019/04/monica.jpg">
            </div>
            <div class="col-sm-12 col-md-6">
                <h3 class="fs-28">Carlota Lucia Rodriguez Duran <span class="fs-16 text-blue">11a</span></h3>
                <p class="fs-14 text-muted-2">Último acceso: 08/06/2020</p>
            </div>
            <div class="col-sm-12 col-md-5 d-flex flex-column justify-content-center">
                <span class="fs-14 text-yellow uppercase bolder">Progreso</span>
                <div class="progress w-100" style="height: 40px;">
                    <div class="progress-bar bg-success fs-18 bolder" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100">45%</div>
                </div>
            </div>
        </div>
        <!-- Termina el loop -->
        <div class="row mb-5 mt-5 mt-md-0 mb-md-0 pt-2 pb-2 profile-item-list">
			<div class="col-sm-12 col-md-1">
				<img class="img-fluid rounded-circle profile-circle-male" src="https://www.amcharts.com/wp-content/uploads/2019/04/chandler.jpg">
            </div>
            <div class="col-sm-12 col-md-6">
                <h3 class="fs-28">Martin Alejandro Caiza Suntaxi <span class="fs-16 text-blue">6a</span></h3>
                <p class="fs-14 text-muted-2">Último acceso: 08/06/2020</p>
            </div>
            <div class="col-sm-12 col-md-5 d-flex flex-column justify-content-center">
                <span class="fs-14 text-yellow uppercase bolder">Progreso</span>
                <div class="progress w-100" style="height: 40px;">
                    <div class="progress-bar bg-success fs-18 bolder" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                </div>
            </div>
        </div>
        <div class="row mb-5 mt-5 mt-md-0 mb-md-0 pt-2 pb-2 profile-item-list">
			<div class="col-sm-12 col-md-1">
				<img class="img-fluid rounded-circle profile-circle-female" src="https://www.amcharts.com/wp-content/uploads/2019/04/rachel.jpg">
            </div>
            <div class="col-sm-12 col-md-6">
                <h3 class="fs-28">Maria Alejandra Vasquez Cisneros <span class="fs-16 text-blue">14a</span></h3>
                <p class="fs-14 text-muted-2">Último acceso: 08/06/2020</p>
            </div>
            <div class="col-sm-12 col-md-5 d-flex flex-column justify-content-center">
                <span class="fs-14 text-yellow uppercase bolder">Progreso</span>
                <div class="progress w-100" style="height: 40px;">
                    <div class="progress-bar bg-success fs-18 bolder" role="progressbar" style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%</div>
                </div>
            </div>
		</div>
	</div>
</div>
@endsection