<!-- ======= Counts Section ======= -->
<section id="counts" class="counts section-bg">
    <div class="container">

      <div class="row counters">

        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">1232</span>
          <p>Estudiantes</p>
        </div>

        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">6</span>
          <p>Libros</p>
        </div>

        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">42</span>
          <p>Instituciones</p>
        </div>

        <div class="col-lg-3 col-6 text-center">
          <span data-toggle="counter-up">81</span>
          <p>Docentes</p>
        </div>

      </div>

    </div>
  </section><!-- End Counts Section -->