<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="descriptison">
    <meta content="" name="keywords">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/main-styles.css')}}">
    <!-- END Stylesheets -->
    <script src="{{asset('js/app.js')}}"></script>
    <!-- Scripts -->
    <title>Editorial Libertad @yield('page_title')</title>
    @yield('head')
</head>
<body>

<!-- ERRORS ALERT -->
<div class="error-container" id="errorDiv">
{{-- @include('includes.validation-error') --}}
{{-- @include('includes.session-flash-status') --}}
{{-- {{ json_encode(session()->all()) }} --}}
{{-- {{ json_encode(Auth::user()) }} --}}
</div>

@yield('body_afteropeningtag')
@include('layouts.frontend.header')
@yield('content_before')
<div class="page-container">
@yield('content')
</div><!-- End page Container -->
@yield('content_after')
@include('layouts.frontend.footer')
@yield('body_beforeclosingtag')
</body>
</html>
