
<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Editorial Libertad</h3>
            <p>
              Jose Padilla y Nuñez de Vela <br>
              Quito, PIC 170105<br>
              Ecuador <br><br>
              <strong>Teléfono:</strong> +593 (2) 2541331<br>
              <strong>Email:</strong> info@example.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Recursos</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Inicio</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Acerca de</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Trabaja con nosotros</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contacto</a></li>              
              <li><i class="bx bx-chevron-right"></i> <a href="#">Políticas de Privacidad</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Términos y condiciones</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Más links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Librería</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Instituciones</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Aliados</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Registrarse</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Entrar</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Suscribase a nuestra Newsletter</h4>
            <p>Manténgase al tanto de todas las novedades que tenemos para usted.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribirme">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>Editorial Libertad</span></strong>. Todos los derechos reservados
        </div>
        <div class="credits">
          Desarrollado por <a href="https://bootstrapmade.com/">CodeMotion</a> y Diseñado por <a href="https://bootstrapmade.com/">Choclomedia</a>
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  
@yield('footer_script_before')
{{-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script> --}}
<script src="assets/js/main.js"></script>
@yield('footer_scripts')
<script>
    var baseUrl = '{{ URL::to('/') }}';
</script>
@yield('footer_script_after')
@yield('footer_after')