<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', function () {
    return view('frontend/home');
})->name('home');

// Páginas Corporativas
Route::get('/acerca-de', 'frontend\FrontendController@acerca_de')->name('acerca_de');
Route::get('/contacto', 'frontend\FrontendController@contacto')->name('contacto');
Route::get('/libreria', 'frontend\FrontendController@libreria')->name('contacto');

// Usuarios
Route::get('/entrar', 'frontend\SuscriptorController@entrar')->name('entrar');
Route::post('/entrar', 'frontend\SuscriptorController@login')->name('post.login-suscriptor');
Route::get('/salir', 'frontend\SuscriptorController@logout')->name('logout-suscriptor');

// Academia
Route::get('/escritorio', 'frontend\FrontendController@dashboard')->name('dashboard');
Route::get('/escritorio/mis-suscripciones', 'frontend\FrontendController@mis_suscripciones')->name('representante-suscripciones');
Route::get('/escritorio/listado-de-tareas', 'frontend\FrontendController@listado_tareas')->name('representante-tareas');
Route::get('/escritorio/listado-de-tests', 'frontend\FrontendController@listado_tests')->name('representante-tests');
Route::get('/escritorio/libros', 'frontend\FrontendController@listado_libros')->name('libros');